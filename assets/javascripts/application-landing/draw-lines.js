/* Draw SVG lines as user scrolls */
$(function () {
  initSVGLine('.path-to-consistency')
  initSVGLine('.path-to-market')
})

/* Get a reference to the <path> */
const initSVGLine = (elClass) => {
  const path = document.querySelector(elClass)

  // Get length of path
  const pathLength = path.getTotalLength()

  // Make very long dashes (the length of the path itself)
  path.style.strokeDasharray = `${pathLength} ${pathLength}`

  // Offset the dashes so the it appears hidden entirely
  path.style.strokeDashoffset = pathLength

  /**
  * Jake Archibald says so
  * https://jakearchibald.com/2013/animated-line-drawing-svg/
  */
  path.getBoundingClientRect()

  /* When the page scrolls... */
  window.addEventListener('scroll', function (e) {
    drawWhenVisible(elClass)
  })

  /* Check if the element is visible on screen */
  const drawWhenVisible = (el) => {
    const scrolled = $(window).scrollTop()
    const distance = $(el).offset().top
    const minOffset = $(window).height() / 2
    let velocity = 8
    const screenWidth = window.screen.width
    const scrollToVisible = distance - $(window).height() + minOffset < 0 ? 0 : distance - $(window).height() + minOffset
    // If the element is visible, then start drawing it
    if (distance - scrolled <= $(window).height() - minOffset) {
      /**
      * What % down is it?
      * https://stackoverflow.com/questions/2387136/cross-browser-method-to-determine-vertical-scroll-percentage-in-javascript/2387222#2387222
      * Had to try three or four differnet methods here. Kind of a cross-browser nightmare.
      * Minus the distance to scroll to the visible area scrollToVisible
      * And accelerate the scrolling by 4
      */
      /* Check for tablet resolution to increase the line draw acceleration */
      if (screenWidth <= 768) {
        velocity = 14
      }

      const scrollPercentage = [(document.documentElement.scrollTop - scrollToVisible + document.body.scrollTop) / (document.documentElement.scrollHeight - document.documentElement.clientHeight)] * velocity

      /* Length to offset the dashes */
      const drawLength = pathLength * scrollPercentage

      /* Draw in reverse */
      path.style.strokeDashoffset = pathLength - drawLength

      /* When complete, remove the dash array, otherwise shape isn't quite sharp */
      /* Accounts for fuzzy math */
      if (scrollPercentage >= 0.99) {
        path.style.strokeDasharray = 'none'
      } else {
        path.style.strokeDasharray = `${pathLength} ${pathLength}`
      }
    } else {
      return false
    }
  }
}
