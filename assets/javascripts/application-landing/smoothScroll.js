/* Smoothscrolling with anchors
   ========================================================================== */
$(document).on('click', '.smoothScroll', function () {
  const target = $(this).data('linkto')

  if (target.length) {
    $('html,body').animate({
      scrollTop: $(`#${target}`).offset().top
    }, 1000)
  }

  return true
})
